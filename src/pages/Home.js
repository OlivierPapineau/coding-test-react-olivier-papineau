import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';


class HomePage extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    { title('Page d\'accueil') }
                </Helmet>

                <div className="home-page content-wrap">
                    <div className="infos-block">
                        <div className="infos-block__title-block">
                            <h1 className="home-page__title infos-block__title-block__full">04h11</h1>
                            <span className="infos-block__title-block__outline">04h11</span>
                        </div>
                        <p className="home-page__subtitle">Spécialiste de vos données.</p>
                    </div>

                    <Link to="/users" className="nav-arrow home-page__arrow">
                        <Icon>arrow_right_alt</Icon>
                    </Link>

                    <figure className="circle circle1"></figure>
                    <figure className="circle circle2"></figure>
                </div>
            </Fragment>
        )
    }
}

export default HomePage;
