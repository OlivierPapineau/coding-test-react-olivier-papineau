import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { BrowserRouter, Route, withRouter, Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';

import { userService } from 'services';
import { SelectItem } from '../components';
import {Dropdown} from '../components';
import { parseDate } from '../utils/dateParse';


class UserPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            list: [],
            id: null,
            user: {},
            loading: true,
        }
    }

    componentDidMount() {
        // userService.list() ... Pour remplir this.state.list
        const {match: {params}} = this.props;
        console.log(params.id);

        userService.list
            .then((resolve) => {
                this.setState({
                    ...this.state, 
                    list: resolve,
                    id: params.id || 1,
                }, () => {
                    this.loadUserInfo(this.state.id);
                });
            });
    }

    loadUserInfo(id) {
        userService.userInfo(id)
            .then((resolve) => {
                this.setState({
                    ...this.state,
                    user: {
                        ...resolve,
                    },
                    loading: false,
                });
            })
    }

    handleSelectChange = async (e) => {
        await this.props.history.push(`/users/${e.target.value}`);
        const {match: {params}} = this.props;
        
        this.setState({
            ...this.state,
            id: params.id,
        }, () => {
            this.loadUserInfo(this.state.id);
        });
    }


    render() {
        const { list, user } = this.state;
        let articlesList = null;
        let birthdate = null;
        if (!this.state.loading) {
            birthdate = parseDate(user.birthdate);
            articlesList = user.articles.map(article => {
                return (
                    <div key={`article${article.id}-user${article.user_id}`} className="user-page__articles-list__article">
                        <h3 className="user-page__articles-list__article__title">{article.name}</h3>
                        <p className="user-page__articles-list__article__content">
                            {article.content}
                        </p>
                    </div>
                );
            });
        }
        return (
            <Fragment>
                <Helmet>
                    { title('Page secondaire') }
                </Helmet>

                <div className="user-page content-wrap">
                    <div className="user-wrap">
                        <Link to="/" className="nav-arrow user-page__arrow">
                            <Icon style={{ transform: 'rotate(180deg)' }}>arrow_right_alt</Icon>
                        </Link>

                        <div className="users-select">
                            <h1>
                                <select defaultValue={user.id} className="users-select__droplist" onChange={this.handleSelectChange}>
                                    { /* Liste dynamique à partir de l'API */ }
                                    {list.map(user => {
                                        return <SelectItem key={`key_user${user.id}`} id={user.id} name={user.name} currentId={user.id}></SelectItem>
                                    })}
                                </select>
                            </h1>
                        </div>

                        <div className="infos-block">
                            { /* Infos dynamiques sur l'utilisateur sélectionné */ }
                            <h2 className="infos-block__info">Occupation: <span className="infos-block__info--light">{user.occupation}</span></h2>
                            <h2 className="infos-block__info">Date de naissance: <span className="infos-block__info--light">{birthdate}</span></h2>
                        </div>

                        
                        <div className="user-page__articles-list">
                            { /* Liste dynamique tirée de l'utilisateur sélectionné */ }
                            {articlesList}
                        </div>
                    </div>
                    
                    
                </div>
            </Fragment>
        )
    }
}

export default UserPage;
