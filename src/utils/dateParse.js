
export const parseDate = (date) => {
    const splitDate = date.split("-");
    const dateObj = new Date(splitDate);
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    };

    return dateObj.toLocaleDateString('fr-FR', options)
}