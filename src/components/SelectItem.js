import React from "react";


export const SelectItem = (props) => {
    const { id, name } = props;
    //const isSelected = id === currentId;

    return (
        <option id={`user-${id}`} value={id}>
            {name}
        </option>
    );
};

