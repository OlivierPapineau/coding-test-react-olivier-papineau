import React from 'react';
import { BrowserRouter, Route, withRouter } from "react-router-dom";

export const Dropdown = (props) => {
    const { users } = props;

    return (
        <select>
            {users.map(user => {
                const { id, name } = user;
                return (
                    <option key={`key-user${id}`} value={id}>
                        {name}
                    </option>
                )
            })}
        </select>
    );
};